import fitparse
import csv
import sys
import gzip
import requests
import os
from tqdm import tqdm


# use arg to choose filename
if len(sys.argv) < 2:
    print("Usage: python Fit_to_CSV.py <filename>")
    sys.exit(1)

full_filename = sys.argv[1]

filename = full_filename.split("/")[-1]
zipped = False

if filename.endswith(".gz"):
    zipped = True
    filename = filename[:-3]
    with gzip.open(full_filename, 'rb') as f_in:
        with open(filename, 'wb') as f_out:
            f_out.writelines(f_in)

# Load the FIT file
fitfile = fitparse.FitFile(filename)

if zipped:
    # remove unziped file
    os.remove(filename)

# format csv => timestamp, latitude, longitude, distance 
csvResult = []
for record in fitfile.get_messages("record"):

    line = record.get_values()

    timestamp = -1
    latitude = -1
    longitude = -1
    distance = -1
    speed = -1

    if 'timestamp' in line:
        timestamp = line['timestamp']

    if 'position_lat' in line:

        # convert to decimal if latitude is not null
        if line['position_lat'] is not None:
            latitude = line['position_lat'] / 11930465

    if 'position_long' in line:
        # convert to decimal if longitude is not null
        if line['position_long'] is not None:
            longitude = line['position_long'] / 11930465

    if 'distance' in line:
        distance = line['distance']

    if 'speed' in line:
        if line['speed'] is not None:
            speed = line['speed']

    # if latitude is None or longitude is None:
    #     continue

    csvResult.append([timestamp, latitude, longitude, distance, speed])

# write result in csv file

filename = filename.split("/")[-1]
csvFilename = filename.replace('.fit', '.csv')
csvFilename = "csv_activities/" + csvFilename

with open(csvFilename, 'w') as csv_file:
    writer = csv.writer(csv_file)

    writer.writerow(['timestamp', 'latitude', 'longitude', 'distance', 'speed', 'elevation'])

    for data in tqdm(csvResult):

        latitude = data[1]
        longitude = data[2]
        elevation = -1

        if latitude != -1 or longitude != -1:

            url = "https://api.open-elevation.com/api/v1/lookup?locations={latitude},{longitude}".format(
                latitude=latitude, longitude=longitude)

            response = requests.get(url)

            if response.status_code == 200:
                elevation = response.json()['results'][0]['elevation']

            data.append(elevation)

            writer.writerow(data)

