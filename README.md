# French version
[Strava](http://p1911165.pages.univ-lyon1.fr/projet-dataviz-strava/)

Le projet `Strava` est une application de visualisation de données basée sur Strava, une plateforme populaire pour le suivi des activités de fitness. Le projet est développé en utilisant d3.js, une bibliothèque JavaScript pour la visualisation de données, Leaflet pour l'affichage de la carte et Python pour le prétraitement des données. 

L'application utilise des données CSV d'activités Strava, qui sont traitées et visualisées de 3 manières différentes : 
- un calendrier des activités de l'année avec un récapitulatif par semaine
- un graphique récapitulatif par catégorie d'activité (2km, 10km, 20km, 30km, Marathon) de l'année ou par mois
- une carte interactive sur laquelle les utilisateurs peuvent voir les lieux les plus visités lors de leurs activités. 

Le calendrier et le graphique récapitulatif ont été totalement créer en utilisant d3.js. La carte, elle, est rendue en utilisant la bibliothèque Leaflet et les données sont traitées en utilisant la bibliothèque d3.js.

![Strava](5-teaser.png)

# English version
[Strava](http://p1911165.pages.univ-lyon1.fr/projet-dataviz-strava/)

The `Strava` project is a data visualization application based on Strava, a popular platform for tracking fitness activities. The project is developed using d3.js, a JavaScript library for data visualization, Leaflet for the map rendering and Python for data preprocessing.

The application uses CSV data from Strava activities, which are processed and visualized in 3 different ways:
- a calendar of the year's activities with a weekly summary
- a summary graph by activity category (2km, 10km, 20km, 30km, Marathon) for the year or by month
- an interactive map on which users can see the most visited places during their activities.

The calendar and the summary graph were entirely created using d3.js. The map is rendered using the Leaflet library and the data is processed using the d3.js library.

![Strava](5-teaser.png)