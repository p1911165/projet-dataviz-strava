[Strava](http://projet-dataviz-strava-p1911165-93278c4edbc641419672d1852c20820a.pages.univ-lyon1.fr/index.html)

The `Strava` project is a data visualization application based on Strava, a popular platform for tracking fitness activities. The project is developed using d3.js, a JavaScript library for data visualization, Leaflet for the map rendering and Python for data preprocessing.

The application uses CSV data from Strava activities, which are processed and visualized in 3 different ways:
- a calendar of the year's activities with a weekly summary
- a summary graph by activity category (2km, 10km, 20km, 30km, Marathon) for the year or by month
- an interactive map on which users can see the most visited places during their activities.

The calendar and the summary graph were entirely created using d3.js. The map is rendered using the Leaflet library and the data is processed using the d3.js library.