[Strava](http://projet-dataviz-strava-p1911165-93278c4edbc641419672d1852c20820a.pages.univ-lyon1.fr/index.html)

Le projet `Strava` est une application de visualisation de données basée sur Strava, une plateforme populaire pour le suivi des activités de fitness. Le projet est développé en utilisant d3.js, une bibliothèque JavaScript pour la visualisation de données, Leaflet pour l'affichage de la carte et Python pour le prétraitement des données. 

L'application utilise des données CSV d'activités Strava, qui sont traitées et visualisées de 3 manières différentes : 
- un calendrier des activités de l'année avec un récapitulatif par semaine
- un graphique récapitulatif par catégorie d'activité (2km, 10km, 20km, 30km, Marathon) de l'année ou par mois
- une carte interactive sur laquelle les utilisateurs peuvent voir les lieux les plus visités lors de leurs activités. 

Le calendrier et le graphique récapitulatif ont été totalement créer en utilisant d3.js. La carte, elle, est rendue en utilisant la bibliothèque Leaflet et les données sont traitées en utilisant la bibliothèque d3.js.