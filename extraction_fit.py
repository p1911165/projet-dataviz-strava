import fitparse
import requests
import os
from tqdm import tqdm


def csv_values(fit_file, chemin_csv):
    # Open the fit file
    fitfile = fitparse.FitFile(fit_file)

    # Create a csv file with the same name
    with open(chemin_csv, 'w', newline='') as csv_file:
        csv_file.write("Time,LatitudeDegrees,LongitudeDegrees,Elevation,Speed,Distance\n")

        for record in fitfile.get_messages("record"):
            line = record.get_values()

            timestamp = None
            latitude = None
            longitude = None
            elevation = None
            speed = None
            distance = None

            if 'timestamp' in line:
                timestamp = line['timestamp']

            if 'position_lat' in line:

                # convert to decimal if latitude is not null
                if line['position_lat'] is not None:
                    latitude = line['position_lat'] / 11930465

            if 'position_long' in line:
                # convert to decimal if longitude is not null
                if line['position_long'] is not None:
                    longitude = line['position_long'] / 11930465

            if 'distance' in line:
                distance = line['distance']

            if 'speed' in line:
                if line['speed'] is not None:
                    speed = line['speed']

            if latitude is not None or longitude is not None:

                url = "https://api.open-elevation.com/api/v1/lookup?locations={latitude},{longitude}".format(
                    latitude=latitude, longitude=longitude)

                response = requests.get(url)

                if response.status_code == 200:
                    elevation = response.json()['results'][0]['elevation']

                csv_file.write(f"{timestamp},{latitude},{longitude},{elevation},{speed},{distance}\n")


if __name__ == "__main__":
    # Dossier contenant les fichiers FIT
    dossier_activites = "export_112198995/activities/"

    # Parcourir les fichiers .fit du dossier
    for dossier, sous_dossiers, fichiers in os.walk(dossier_activites):
        fichiers = [fichier for fichier in fichiers if fichier.endswith(".fit")]
        for fichier in tqdm(fichiers):
            # Chemin complet du fichier fit
            chemin_fichier_fit = os.path.join(dossier, fichier)

            # Créer le chemin pour le fichier CSV en remplaçant l'extension
            chemin_fichier_csv = os.path.splitext(chemin_fichier_fit)[0] + ".csv"

            if os.path.exists(chemin_fichier_csv):
                continue
            # Appliquer la fonction pour extraire les données
            csv_values(chemin_fichier_fit, chemin_fichier_csv)
