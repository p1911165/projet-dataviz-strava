const latitude = 45.78;
const longitude = 4.85;
const zoomLevel = 12;

// Initialize Leaflet map
const map = L.map('map').setView([latitude, longitude], zoomLevel);

// Add a base map layer (e.g., OpenStreetMap)
//L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(map);

if(document.location.href.includes("http://127.0.0.1")) {
    L.tileLayer('https://tiles.stadiamaps.com/tiles/alidade_smooth/{z}/{x}/{y}{r}.png').addTo(map);
} else {
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(map);
}

map.setMaxZoom(15); 
map.dragging.disable();


d3.csv("csv_activities/file.csv").then( function(data) {


    data = data.map(d => "csv_activities/" + d.filename);


    Promise.all(
        data.map(d => d3.csv(d))
    ).then( 
        function(data) {

            // 2D to 1D array
            data = data.flat();

            // keep only coords
            data = data.map(d => ({latitude: parseFloat(d.LatitudeDegrees), longitude: parseFloat(d.LongitudeDegrees)}));            
            
            // filter on Nans
            data = data.filter(d => !isNaN(d.latitude) && !isNaN(d.longitude));
            

            drawMostVisitedPlacesContour(data);

        }
    );

})



document.getElementById("map_opacity_slider").oninput = function() {

    let value = parseFloat(this.value);

    document.getElementById("map_opacity_value").innerHTML = Math.round(value*100) + "%";
    
    document.getElementsByClassName("leaflet-tile-pane")[0].style.opacity = value;


}




function coordToMapPx(data) {
    
        let coordspoints = Array();
    
        data.forEach(function(d) {
    
            let coords = map.latLngToContainerPoint([d.latitude, d.longitude]);

            if(coords.x > map.getSize().x || coords.y > map.getSize().y || coords.x < 0 || coords.y < 0) {
                return;
            }

            coordspoints.push({x:coords.x, y:coords.y});
        
        });

        return coordspoints;
}




function drawMostVisitedPlacesContour(data) {



    let coordspoints = coordToMapPx(data);

   



    const svg = d3.select(map.getPanes().overlayPane)
                    .append("svg")
                    .attr("width", map.getSize().x)
                    .attr("height", map.getSize().y);

    const g = svg.append("g").attr("class", "leaflet-zoom-hide");


    var color = d3.scaleLinear()
                .domain([0, 3])
                .range(["green", "red"]);



    const ct = d3.contourDensity()
                    .x(d => d.x)
                    .y(d => d.y)
                    .size([map.getSize().x, map.getSize().y])
                    .bandwidth(20);

    var densityData = ct(coordspoints);


    var path = g.selectAll("path")
                .data(densityData)
                .enter()
                .append("path")
                .attr("d", d3.geoPath())
                .attr("fill", d=> color(d.value))
                .attr("opacity", 0.1);


    map.on("viewreset", update);
    map.on("zoom", zoomUpdate);
    
    function zoomUpdate() {

        coordspoints = coordToMapPx(data);

        densityData = ct(coordspoints);

        color = d3.scaleLinear()
                  .domain([0, d3.max(densityData, d => d.value)])
                  .range(["green", "red"]);


        path.data(densityData)
            .attr("d", d3.geoPath())
            .attr("fill", d=> color(d.value))
            .attr("opacity", 0.1);
    }

    function update() {

        coordspoints = coordToMapPx(data);

        densityData = ct(coordspoints);

        color = d3.scaleLinear()
                  .domain([0, d3.max(densityData, d => d.value)])
                  .range(["green", "red"]);


        path.data(densityData)
            .attr("d", d3.geoPath())
            .attr("fill", d=> color(d.value))
            .attr("opacity", 0.1);
    }

    
}

