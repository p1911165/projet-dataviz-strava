var params = new URLSearchParams(window.location.search);
var activityId = params.get('activityId'); 

const latitude = 45.7;
const longitude = 4.8;
const zoomLevel = 13;

// Initialize Leaflet map
const map = L.map('map').setView([latitude, longitude], zoomLevel);


if(document.location.href.includes("http://127.0.0.1")) {
    L.tileLayer('https://tiles.stadiamaps.com/tiles/alidade_smooth/{z}/{x}/{y}{r}.png').addTo(map);
} else {
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(map);
}




// set the dimensions and margins of the graph
const margin = {top: 30, right: 30, bottom: 30, left: 50},
      width = window.innerWidth - margin.left - margin.right,
      height = 200 - margin.top - margin.bottom;







// append the svg object to the body of the page
const svg = d3.select("#elevation_plot")
    .append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", `translate(${margin.left},${margin.top})`);










// get the data
d3.csv("export_112198995/activities.csv").then( function(data) {

    // find activity by id
    let activity = data.filter(d => d["Activity ID"] == activityId);

    if(activity.length == 0) {
        alert("No activity found");
        return;
    }

    activity = activity[0];


    addTextInfo(activity);


    let activity_filename = "csv_activities/"+activity.Filename.split("/")[1].split(".")[0] + ".csv";

    d3.csv(activity_filename).then( function(activity_data) {


        activity_data = dataFormating(activity_data);
        drawMapPath(activity_data);
        drawElevationProfile(activity_data);
    })
});




// ========= FUNCTIONS =========


function dataFormating(activity_data) {
    const startDate = new Date(activity_data[0].Time);


    activity_data.forEach(function(d) {


        d.Time = timestampFormating(startDate, new Date(d.Time));
        d.Distance = parseInt(d.Distance);
        d.Speed = decimalPaceToTimeString(d.Speed);

        d.LatitudeDegrees = parseFloat(d.LatitudeDegrees);
        d.LongitudeDegrees = parseFloat(d.LongitudeDegrees);
        d.Elevation = parseFloat(d.Elevation);
    });


    // if elevation is NaN, interpolate previous not nan value and next not nan value
    for(let i=0; i<activity_data.length; i++) {
        if(isNaN(activity_data[i].Elevation)) {

            let id_previous = i-1;

            while(id_previous > 0 && isNaN(activity_data[id_previous].Elevation)) {
                id_previous--;
            }

            let id_next = i+1;

            while(id_next < activity_data.length && isNaN(activity_data[id_next].Elevation)) {
                id_next++;
            }


            if(id_previous == -1) {
                activity_data[i].Elevation = activity_data[id_next].Elevation;
            } else if(id_next == activity_data.length) {
                activity_data[i].Elevation = activity_data[id_previous].Elevation;
            } else {
                activity_data[i].Elevation = (activity_data[id_previous].Elevation + activity_data[id_next].Elevation) / 2;
            }
        }
    }



    return activity_data;
}


function decimalPaceToTimeString(pace) {
    pace = parseFloat(pace);
    let pace_min = Math.floor(pace);
    let pace_sec = Math.floor((pace - pace_min) * 60);

    if(pace_sec < 10) {
        pace_sec = "0"+pace_sec;
    }

    return pace_min+":"+pace_sec;
}





function addTextInfo(activity) {
    // ===== Title ======
    let date = new Date(activity["Activity Date"]);
    date = date.toLocaleString('fr-FR', { day: '2-digit', month: 'long', year: 'numeric', hour: '2-digit', minute: '2-digit' });
    
    document.getElementById("activity_info").innerHTML = activity["Activity Name"] + " le " + date;


    // ====== Stat section ======
    // activity duration 
    let duration = parseFloat(activity["Moving Time"]);

    let duration_hour = Math.floor(duration/(60*60));
    let duration_min = Math.floor(duration/(60) - duration_hour*60);
    let duration_sec = Math.floor(duration - duration_hour*60*60 - duration_min*60);


    duration_hour = duration_hour >0 ? duration_hour+":" : "";
    duration_min = duration_min < 10 ? "0"+duration_min : duration_min;
    duration_sec = duration_sec < 10 ? "0"+duration_sec : duration_sec;


    document.getElementById("time").innerHTML = duration_hour + duration_min + ":" + duration_sec;

    // pace 

    let pace = parseFloat(activity["Average Speed"]);
    let min = Math.floor(pace);
    let sec = Math.floor((pace - min) * 60);

    document.getElementById("speed").innerHTML =  min+":"+sec+ " /km";

    // distance 
    document.getElementById("dist").innerHTML = parseFloat(activity["Distance"]/1000).toFixed(2) + "km";

    // elevation
    document.getElementById("elevation").innerHTML = parseFloat(activity["Elevation Gain"]).toFixed(2) + " m";
}




function createIcon(iconUrl) {
    return L.icon({
      iconUrl: iconUrl,
      iconSize: [14, 14],
      iconAnchor: [7, 7],
    });
  }




function drawMapPath(data) {

    const centerLat = (Math.max(...data.map(d => d.LatitudeDegrees)) + Math.min(...data.map(d => d.LatitudeDegrees))) / 2;
    const centerLng = (Math.max(...data.map(d => d.LongitudeDegrees)) + Math.min(...data.map(d => d.LongitudeDegrees))) / 2;

    map.setView([centerLat, centerLng], zoomLevel);

    L.polyline(data.map(d=> [d.LatitudeDegrees, d.LongitudeDegrees, d.Elevation]), { color: 'red' }).addTo(map);


    // add start and finish icons
    const startIcon = createIcon('assets/start.png');
    const finishIcon = createIcon('assets/finish.png');

    var start = L.marker(
        [data[0].LatitudeDegrees, data[0].LongitudeDegrees],
        { icon: startIcon }
      ).addTo(map);
  
  
    var finish = L.marker(
        [data[data.length - 1].LatitudeDegrees, data[data.length - 1].LongitudeDegrees],
        { icon: finishIcon }
    ).addTo(map);

    

    // Update icon size based on map zoom level
    map.on('zoomend', function () {
        const zoom = map.getZoom();
        const scale = zoom / zoomLevel; // Adjust the scale based on your needs
        const iconSize = [12 * scale, 12 * scale];
        startIcon.options.iconSize = iconSize;
        finishIcon.options.iconSize = iconSize;
        start.setIcon(startIcon);
        finish.setIcon(finishIcon);
    });

}


function getAxisX(data) {
    let x = d3.scaleLinear()
        .domain(d3.extent(data, function(d) { return d.Distance; }))
        .range([0, width]);

    // draw lines
    svg.append("g")
        .attr("transform", `translate(0, ${height})`)
        .call(d3.axisBottom(x)
                .tickSize(-height)
                .tickFormat(''))
        .attr("opacity", 0.2);

    // draw values
    svg.append("g")
        .attr("transform", `translate(0, ${height})`)
        .call(d3.axisBottom(x)
                .tickFormat(d=> `${d/1000},0 km`));

    
    return x;
}


function getAxisElevationY(data) {
    const minElevation = d3.min(data, function(d) { return d.Elevation; });
    const maxElevation = d3.max(data, function(d) { return d.Elevation; });

    const elevationMargin = (maxElevation - minElevation) * 0.2;

    const domainMin = Math.floor((minElevation - elevationMargin) / 10) * 10; // minElevation - elevationMargin;
    const domainMax = Math.ceil((maxElevation + elevationMargin) / 10) * 10; // maxElevation + elevationMargin;
    

    const y = d3.scaleLinear()
        .range([height, 0])
        .domain([domainMin, domainMax]); 


    // draw lines
    svg.append("g")
        .call(d3.axisLeft(y)
                .ticks(4)
                .tickSize(-width)
                .tickFormat(''))
        .attr("opacity", 0.2);

    // draw values
    svg.append("g")
        .call(d3.axisLeft(y)
                .ticks(4)
                .tickFormat(d=> `${d} m`));

    return y;
}



function drawElevationProfile(data) {
    const x = getAxisX(data);
    const y_elevation = getAxisElevationY(data);
    
    // Compute the area
    var area = d3.area()
        .x(function(d) { return x(d.Distance); })
        .y0(height)
        .y1(function(d) { return y_elevation(d.Elevation); });


    // Draw the area
    svg.append("path")
        .datum(data)
        .attr("class", "area")
        .attr("d", area)
        .attr("fill", "#aaaaaa")
        .attr("stroke", "#000")
        .attr("stroke-width", 1)
        .attr("opacity", 0.5);


    // Add a marker that follows the mouse on elevation profile
    const cursorIcon = createIcon('assets/cursor.png');

    var marker = L.marker(
        [data[0].LatitudeDegrees, data[0].LongitudeDegrees],
        { icon: cursorIcon }
      ).addTo(map);

    marker.setOpacity(0); // marker is initially invisible

    
    var tooltip = d3.select("#tooltip");


    // Append a transparent rectangle to the SVG
    svg.append("rect")
        .attr("width", width)
        .attr("height", height)
        .style("fill", "none")
        .style("pointer-events", "all") // This line is important
        .on("mousemove", function(event) {

            svg.selectAll(".mouseLine").remove();

            svg.append("line")
                .attr("x1", event.offsetX-50)
                .attr("x2", event.offsetX-50)
                .attr("y1", 0)
                .attr("y2", height)
                .attr("stroke", "red")
                .attr("stroke-width", 1)
                .attr("opacity", 0.5)
                .attr("class", "mouseLine");


            // Get the mouse position relative to the SVG
            const [mouseX] = d3.pointer(event);
            

            // Use the invert function of the x scale to get the distance
            const distance = x.invert(mouseX);

            // Find the data point that is closest to this distance
            const closestDataPoint = data.reduce((prev, curr) => 
                Math.abs(curr.Distance - distance) < Math.abs(prev.Distance - distance) ? curr : prev
            );

        
            marker.setOpacity(1);
            marker.setLatLng([closestDataPoint.LatitudeDegrees, closestDataPoint.LongitudeDegrees]);
            
            

            tooltip.style("opacity", 1);
            tooltip.style("pointer-events", "none");

            tooltip.html(`<b>${closestDataPoint.Time}</b>
                         <br/>Alt. ${closestDataPoint.Elevation} m
                         <br/>Dist. ${(closestDataPoint.Distance / 1000).toFixed(1)} km
                         <br/>Allure. ${closestDataPoint.Speed} /km`);

                         



            // if position of tooltip doesnt fit, put it to the left of the line
            const XrectBorder = this.getBoundingClientRect().x + this.getBoundingClientRect().width;
            const tooltipWidth = tooltip.node().getBoundingClientRect().width;
            let tooltipX = event.pageX + 5;
    
            if(tooltipX + tooltipWidth > XrectBorder - 5) {
                tooltipX = event.pageX - tooltipWidth - 5;
            }

            tooltip.style("left", tooltipX + "px");
            tooltip.style("top", this.getBoundingClientRect().y+20+"px");

        })
        .on("mouseout", function(event) {
            // sleep for 5 ms
            // setTimeout(() => {
            //     svg.selectAll(".mouseLine").remove();
            //     marker.setOpacity(0);
            //     tooltip.style("opacity", 0);
            // }, 500);
            
            
        });
       

}





function timestampFormating(startDate, date) {
    let duration = "0s";

    if(date instanceof Date && !isNaN(date.getTime())) {
        date = date.toISOString();
    
        let date_diff = new Date(date) - startDate;


        let duration_hours = Math.floor(date_diff / (1000 * 60 * 60));
        let duration_minutes = Math.floor((date_diff % (1000 * 60 * 60)) / (1000 * 60));
        let duration_seconds = Math.floor((date_diff % (1000 * 60)) / 1000);

    


        if(duration_hours == 0) {
            if(duration_minutes == 0) {
                duration = `${duration_seconds}s`;
            } else {
                duration = `${duration_minutes}:${duration_seconds < 10 ? '0' : ''}${duration_seconds}`;
            }

        } else {
            duration = `${duration_hours}:${duration_minutes < 10 ? '0' : ''}${duration_minutes}:${duration_seconds < 10 ? '0' : ''}${duration_seconds}`;
        }


        if(duration_hours == 0 && duration_minutes == 0) {
            duration = `${duration_seconds}s`;
        }

    } 

    return duration;
}








