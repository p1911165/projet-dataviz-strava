var url = "export_112198995/activities.csv"
   
      var nb_2km = 0;
      var nb_10km = 0;
      var nb_20km = 0;
      var nb_30km = 0;
      var nb_40km = 0;
      var nb_marathon = 0;

      noeud = [];
      nodes = [];

      //vue par défaut
      var currentView = 'year';
      changeView(currentView);

      function resetAll() {
         resetView();
         resetData();
      }

      function resetView() {
         d3.select("#force").selectAll("svg").remove();
         d3.select("#force").selectAll("div").remove();
         d3.select("#force").selectAll("table").remove();
         d3.select("#legend").selectAll("div").remove();
      }

      function resetData() {
         nb_2km = 0
         nb_10km = 0
         nb_20km = 0
         nb_30km = 0
         nb_40km = 0
         nb_marathon = 0
         nodes = [];
      }

      // Fonction pour changer le mode d'affichage
      function changeView(view) {
         resetAll();
         currentView = view;
         if(view == 'year') {
            updateGraphYear();  
         } else if (view == 'month') {
            updateGraphMonth();
         }
      }

      //ajouter un tableau html à la balise force
      function createTab() {
         d3.select("#force").append("table")
            .attr("id", "tableau")
            //ajouter une ligne au tableau "nb_2km", "nb_10km", "nb_20km", "nb_30km", "nb_40km", "nb_marathon"
            .append("tr")
            .attr("id", "ligne")
            .selectAll("td")
            .data(["svg2km", "svg10km", "svg20km", "svg30km", "svg40km", "marathon"])
            .enter()
            .append("td")
            //ajouter un id
            .attr("id", function(d) { return "titre" + d; })
            .style("text-align", "center")
      };

      function updateGraphYear(){ 

         resetAll();

         d3.csv(url).then( function(data) {
            calculeTypeCourse(data);
            data = ["svg2km", "svg10km", "svg20km", "svg30km", "svg40km", "marathon"];
            createTab();
            data.forEach(function(d) {
               
               checkifnull(d);
               putTitre(d);
               conditionnalCreateSvg(d);
               noeud = createUniqueNodes(d);
               setupSimulation(noeud,d);

            });
         });
      }

      // si il n'est pas égal à 0, alors on crée le svg
      function conditionnalCreateSvg(d) {
         // si il n'est pas égal à 0, alors on crée le svg
         if (d == "svg2km" && nb_2km != 0) {
            createSvg(d);
         } else if (d == "svg10km" && nb_10km != 0) {
            createSvg(d);
         } else if (d == "svg20km" && nb_20km != 0) {
            createSvg(d);
         } else if (d == "svg30km" && nb_30km != 0) {
            createSvg(d);
         } else if (d == "svg40km" && nb_40km != 0) {
            createSvg(d);
         } else if (d == "marathon" && nb_marathon != 0) {
            createSvg(d);
         }
      }
      
 

      function checkifnull(d){
         //si la distance est égale à 0, alors on supprime la colonne du tableau
         if (d == "svg2km" && nb_2km == 0) {
            d3.select("#titre" + d).remove();
            d3.select("#" + d).remove();
         } else if (d == "svg10km" && nb_10km == 0) {
            d3.select("#titre" + d).remove();
            d3.select("#" + d).remove();
         } else if (d == "svg20km" && nb_20km == 0) {
            d3.select("#titre" + d).remove();
            d3.select("#" + d).remove();
         } else if (d == "svg30km" && nb_30km == 0) {
            d3.select("#titre" + d).remove();
            d3.select("#" + d).remove();
         } else if (d == "svg40km" && nb_40km == 0) {
            d3.select("#titre" + d).remove();
            d3.select("#" + d).remove();
         } else if (d == "marathon" && nb_marathon == 0) {
            d3.select("#titre" + d).remove();
            d3.select("#" + d).remove();
         }
      };


      function updateGraphMonth(){
         resetAll();

         // ouvrir le fichier csv sans d3
         d3.csv(url).then( function(data) {
            let monthsData = [];

            // Pour chaque mois, extrayez les données
            data.forEach(function(d) {
                  const date = new Date(d['Activity Date']);
                  const month = date.toLocaleString('en-US', { month: 'long' }); // 'long' pour obtenir le nom complet du mois
                  const index = monthsData.findIndex(item => item.month === month);

                  // Si le mois n'est pas déjà dans le tableau, ajoutez-le
                  if (index === -1) {
                     monthsData.push({
                        month: month,
                        data: [d]
                     });
                  } else {
                     // Sinon, ajoutez les données à ce mois
                     monthsData[index].data.push(d);
                  }
            });

               
               createTab_month();
               let largeur = 110;
               let hauteur = 200;
               monthsData = fixeOrderMonth(monthsData);
               monthsData.forEach(function(d) {
                 
                  noeud = [];
                  resetData();
                  calculeTypeCourse(d.data);
                  createSvg(d.month, largeur, hauteur, true);
                  noeud = createNodes();
                  setupSimulation(noeud,d.month, largeur, hauteur);

               });

               

         });
         // afficher la légende
         createLegend();


      };

      function fixeOrderMonth (data) {
         // rangez les mois dans l'ordre
         new_months_data = [];
         data.forEach(function(d) {
            switch (d.month) {
               case "January":
                  new_months_data[0] = d;
                  break;
               case "February":
                  new_months_data[1] = d;
                  break;
               case "March":
                  new_months_data[2] = d;
                  break;
               case "April":
                  new_months_data[3] = d;
                  break;
               case "May":
                  new_months_data[4] = d;
                  break;
               case "June":
                  new_months_data[5] = d;
                  break;
               case "July":
                  new_months_data[6] = d;
                  break;
               case "August":
                  new_months_data[7] = d;
                  break;
               case "September":
                  new_months_data[8] = d;
                  break;
               case "October":
                  new_months_data[9] = d;
                  break;
               case "November":
                  new_months_data[10] = d;
                  break;
               case "December":
                  new_months_data[11] = d;
                  break;
               default:
                  break;
            }
            
         });
         // si un mois n'est pas dans le tableau, on le rajoute avec un tableau vide
         for (let i = 0; i < 12; i++) {
                  if (new_months_data[i] == undefined) {
                     new_months_data[i] = { month: "rien", data: [] };
                  }
               }

               
         return new_months_data;
      }


      function print_nb_course() {
         console.log("2km : " + nb_2km);
         console.log("10km : " + nb_10km);
         console.log("20km : " + nb_20km);
         console.log("30km : " + nb_30km);
         console.log("40km : " + nb_40km);
         console.log("marathon : " + nb_marathon);
      }

      function createTab_month() {
         d3.select("#force").append("table")
            .attr("id", "tableau")
            .append("tr")
            .attr("id", "ligne")
            .selectAll("td")
            .data(["Jan", "Fév", "Mars", "Avr", "Mai", "Juin", "Juil", "Aout", "Sept","Oct", "Nov", "Déc"])
            .enter()
            .append("td")
            .attr("id", function(d) { return "titre" + d; })
            // ajouter le texte dans une balise h2
            .append("h3")
            .text(function(d) { return d; })
            .style("text-align", "center");

      };
   
      function calculeTypeCourse(data) {
         data.forEach(function(d) {
            d.distance = parseInt(d.Distance);
   
            // Calcule des différents types de courses
            if (d.distance < 2000) {
               nb_2km += 1;
            } else if (d.distance < 10000) {
               nb_10km += 1;
            } else if (d.distance < 20000) {
               nb_20km += 1;
            } else if (d.distance < 30000) {
               nb_30km += 1;
            } else if (d.distance < 40000) {
               nb_40km += 1;
            } else if (d.distance >= 42000) {
               nb_marathon += 1;
            }
         });
      } 

      function putTitre(d) {
         d3.select("#titre" + d).append("h1")
            // switch case
            switch (d) {
               case "svg2km":
                  d3.select("#titre" + d).append("h2").text(nb_2km + " fois 2km");
                  break;
               case "svg10km":  
                  d3.select("#titre" + d).append("h2").text(nb_10km + " fois 10km");
                  break;
               case "svg20km":
                  d3.select("#titre" + d).append("h2").text(nb_20km + " fois 20km");
                  break;
               case "svg30km":
                  d3.select("#titre" + d).append("h2").text(nb_30km + " fois 30km");
                  break;
               case "svg40km":
                  d3.select("#titre" + d).append("h2").text(nb_40km + " fois 40km");
                  break;
               case "marathon":
                  d3.select("#titre" + d).append("h2").text(nb_marathon + " fois un marathon");
                  break;
               default:
                  break;
      }
   };


      function createSvg(d, largeur = 200, hauteur = 300, toolTip = false) {
         // ajouter une case au tableau et mettre un svg dedans
         d3.select("#tableau").append("td")
            .attr("width", largeur)
            .attr("height", hauteur)
            .attr("class", d+"tab")
            .attr("border", 1)
            .attr("align", "center")
            .attr("valign", "center")
            .append("svg")
            .attr("width", largeur)
            .attr("height", hauteur)
            .attr("id", d);
         
         if (toolTip) {
            // ajouter un tooltip
            d3.select("#" + d).append("title")
               .attr("id", "tooltip")
               // afficher le nombre de différentes courses dans le mois
               .text(createTexteTooltip());
         }


      }

      function createTexteTooltip() {
         let texte = "Dans le mois vous avez fait :\n";
         if (nb_2km != 0) {
            texte += "- " + nb_2km + " fois 2km\n";
         }
         if (nb_10km != 0) {
            texte += "- " + nb_10km + " fois 10km\n";
         }
         if (nb_20km != 0) {
            texte += "- " + nb_20km + " fois 20km\n";
         }
         if (nb_30km != 0) {
            texte += "- " + nb_30km + " fois 30km\n";
         }
         if (nb_40km != 0) {
            texte += "- " + nb_40km + " fois 40km\n";
         }
         if (nb_marathon != 0) {
            texte += "- " + nb_marathon + " fois un marathon\n";
         }
         return texte;
      }

      function createUniqueNodes(type) {
         nodes = [];
         // switch case
         
         switch (type) {
            case "svg2km":
               for (let i = 0; i < nb_2km; i++) {
                  nodes.push({ id: i, radius:5, type: "2km" });
               }
               break;
            case "svg10km":
               for (let i = 0; i < nb_10km; i++) {
                  nodes.push({ id: i, radius:11.2, type: "10km" });
               }
               break;
            case "svg20km":
               for (let i = 0; i < nb_20km; i++) {
                  nodes.push({ id: i, radius: 15.8, type: "20km" });
               }
               break;
            case "svg30km":
               for (let i = 0; i < nb_30km; i++) {
                  nodes.push({ id: i, radius: 19.4, type: "30km" });
               }
               break;
            case "svg40km":
               for (let i = 0; i < nb_40km; i++) {
                  nodes.push({ id: i, radius: 22.4, type: "40km" });
               }
               break;
            case "marathon":
               for (let i = 0; i < nb_marathon; i++) {
                  nodes.push({ id: i, radius: 23, type: "marathon" });
               }
               break;
            default:
               break;
         }
         return nodes;
      }
   
      function createNodes() {
         for (let i = 0; i < nb_2km; i++) {
            nodes.push({ id: i, radius: 5, type: "2km" });
         }
         for (let i = 0; i < nb_10km; i++) {
            nodes.push({ id: i + nb_2km, radius: 11.2, type: "10km" });
         }
         for (let i = 0; i < nb_20km; i++) {
            nodes.push({ id: i + nb_2km + nb_10km, radius: 15.8, type: "20km" });
         }
         for (let i = 0; i < nb_30km; i++) {
            nodes.push({ id: i + nb_2km + nb_10km + nb_20km, radius: 19.4, type: "30km" });
         }
         for (let i = 0; i < nb_40km; i++) {
            nodes.push({ id: i + nb_2km + nb_10km + nb_20km + nb_30km, radius: 22.4, type: "40km" });
         }
         for (let i = 0; i < nb_marathon; i++) {
            nodes.push({ id: i + nb_2km + nb_10km + nb_20km + nb_30km + nb_40km, radius: 23, type: "marathon" });
         }
         return nodes;
      }
   
      // Mise en place de la simulation de force
      function setupSimulation(nodes, d, largeur = 200, hauteur = 300) {
         const simulation = d3.forceSimulation(nodes)
         
            .force("charge", d3.forceManyBody().strength(10))
            .force("center", d3.forceCenter(largeur/2, hauteur/2).strength(1))
            .force("collision", d3.forceCollide().radius(d => d.radius ))
            .force("confinement", () => {
            nodes.forEach(d => {
                  d.x = Math.max(d.radius, Math.min(largeur - d.radius, d.x));
                  d.y = Math.max(d.radius, Math.min(hauteur - d.radius, d.y));
            });
               });
            
   
         // Création des cercles dans l'élément SVG
         const svg = d3.select("#" + d);
         const circles = svg.selectAll("circle")
            .data(nodes)
            .enter().append("circle")
            .attr("r", d => d.radius)
            .attr("fill", d => getColor(d.type))
            .call(d3.drag()
               .on("start", dragstarted)
               .on("drag", dragged)
               .on("end", dragended));
   
         // Mise à jour des positions des cercles à chaque itération de la simulation
         simulation.on("tick", () => {
            circles
               .attr("cx", d => Math.max(d.radius, Math.min(largeur - d.radius, d.x)))
               .attr("cy", d => Math.max(d.radius, Math.min(hauteur - d.radius, d.y)));
         });
   
         // Fonctions de glisser-déposer
         function dragstarted(event, d) {
            if (!event.active) simulation.alphaTarget(0.3).restart();
            d.fx = d.x;
            d.fy = d.y;
         }
   
         function dragged(event, d) {
            d.fx = event.x;
            d.fy = event.y;
         }
   
         function dragended(event, d) {
            if (!event.active) simulation.alphaTarget(0);
            d.fx = null;
            d.fy = null;
         }
      }
   
      // Fonction pour obtenir la couleur en fonction du type
      function getColor(type) {
         switch (type) {
            
            case "2km": return "#9467bd";
            case "10km": return "#ff7f0e";
            case "20km": return "#2ca02c";
            case "30km": return "#d62728";
            case "40km": return "#bcbd22";
            case "marathon": return "#17becf";
            default: return "gray";
            /*
            case "2km": return "#6e40aa";
            case "10km": return "#bf3caf";
            case "20km": return "#fe4b83";
            case "30km": return "#ff7847";
            case "40km": return "#e2b72f";
            case "marathon": return "#aff05b";
            default: return "gray";
            */
            
            //https://observablehq.com/@d3/color-schemes?collection=@d3/d3-scale-chromatic
            //["#6e40aa","#bf3caf","#fe4b83","#ff7847","#e2b72f","#aff05b"]
         }
      }
   
      // Création de la légende
      function createLegend() {
         d3.select("#legend").attr("class", "legend");
         // selectionner la class legend
         const legend = d3.select(".legend");
         
   
         const types = ["2km", "10km", "20km", "30km", "40km", "marathon"];
         const legendItems = legend.selectAll(".legend-item")
            .data(types)
            .enter().append("div")
            .attr("class", "legend-item");
   
         legendItems.append("div")
            .attr("class", "legend-color")
            .style("background-color", d => getColor(d));
   
         legendItems.append("div")
            .attr("class", "legend-label")
            .text(d => d);
      }
   

