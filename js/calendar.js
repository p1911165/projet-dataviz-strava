var url = "export_112198995/activities.csv"

function drawCalendar() {
    d3.csv(url).then(function (data) {
        data.forEach(function (d) {
            d['Activity Date'] = new Date(d['Activity Date'])
            d.day = getDayOfYear(d['Activity Date']);
            d.week = getWeekOfYear(d['Activity Date']);
        });

        //Creation des données pour les jours manquants de l'année
        let minDate = new Date(data[data.length - 1]['Activity Date'].getFullYear(), 0, 0);
        let maxDate = new Date(data[data.length - 1]['Activity Date'].getFullYear(), 11, 31);
        let currentDate = minDate;
        let missingDays = [];
        while (currentDate < maxDate) {
            if (!data.find(d => getDayOfYear(d['Activity Date']) === getDayOfYear(currentDate))) {
                missingDays.push({
                    'Activity Date': currentDate,
                    'Distance': 0,
                    'day': getDayOfYear(currentDate),
                    'week': getWeekOfYear(currentDate)
                });
            }
            currentDate = d3.timeDay.offset(currentDate, 1);
        }
        data = data.concat(missingDays);

        maxDistance = d3.max(data, d => parseFloat(d.Distance));
        // Color scale en fonction de la distance parcourue
        const colorScale = d3.scaleLinear()
            .domain([0, maxDistance / 4, maxDistance / 2, maxDistance * 3 / 4, maxDistance])
            .range(["#ececef", "#b4c4b5", "#7e9b82", "#487453", "#094e28"]);

        let width = Math.min(d3.select("#nb_activities").node().getBoundingClientRect().width, 15 * 365 / 7);
        const numRow = 7;
        const totalCell = 365;
        const cellSize = width / (totalCell / numRow) - 1;
        const height = cellSize * (numRow + 2);

        d3.select("#nb_activities").selectAll("svg").remove();
        const svg = d3.select("#nb_activities")
            .append("svg")
            .attr("width", width)
            .attr("height", height)

        var months = [{month: "Jan"}, {month: "Fev"}, {month: "Mar"}, {month: "Avr"}, {month: "Mai"}, {month: "Juin"}, {month: "Juil"}, {month: "Aout"}, {month: "Sept"}, {month: "Oct"}, {month: "Nov"}, {month: "Dec"}];

        var days = [{day: "Lun"}, {day: "Mar"}, {day: "Mer"}, {day: "Jeu"}, {day: "Ven"}, {day: "Sam"}, {day: "Dim"}];

        const xScale = d3.scaleBand()
            .domain(months.map(function (d) {
                return d.month;
            }))
            .range([0, cellSize * (Math.ceil(totalCell / numRow))])

        const xScaleBottom = d3.scaleBand()
            .range([0, cellSize * (Math.ceil(totalCell / numRow))])

        const yScale = d3.scaleBand()
            .domain(days.map(function (d) {
                return d.day;
            }))
            .range([0, height - 2 * cellSize])

        const yScaleWeek = d3.scaleBand()
            .domain(["Recap"])
            .range([0, cellSize])

        const xAxis = d3.axisTop(xScale).ticks(12).tickSize(0);
        const xAxisBottom = d3.axisBottom(xScaleBottom).tickSize(0);
        const yAxis = d3.axisLeft(yScale).ticks(7).tickSize(0);
        const yAxisWeek = d3.axisLeft(yScaleWeek).ticks(2).tickSize(0)

        svg.append("g")
            .attr("class", "x axis")
            .call(xAxis);

        svg.append("g")
            .attr("class", "axis bottom")
            .call(xAxisBottom);

        svg.append("g")
            .attr("class", "y axis")
            .call(yAxis);

        svg.append("g")
            .attr("class", "axisweek")
            .call(yAxisWeek)
            .call(g => g.select(".domain").remove())

        var x = document.getElementsByClassName("x axis")[0].getBoundingClientRect();
        var y = document.getElementsByClassName("y axis")[0].getBoundingClientRect();
        var y_week = document.getElementsByClassName("axisweek")[0].getBoundingClientRect();
        var x_height = x.height;
        var y_width = y.width;
        var y_week_width = y_week.width;
        y_width = Math.max(y_width, y_week_width);

        d3.select(".x.axis")
            .style("transform", "translate(" + y_width + "px," + x_height + "px)")

        d3.select(".axis.bottom")
            .style("transform", "translate(" + y_width + "px," + (height - cellSize / 2 - 2) + "px)")

        d3.select(".y.axis")
            .style("transform", "translate(" + y_width + "px," + x_height + "px)")

        d3.select(".axisweek")
            .style("transform", "translate(" + y_width + "px," + (height - 2) + "px)")

        svg.attr("width", width + y_width)
            .attr("height", height + x_height);

        // tooltip
        var tooltip = d3.select("#nb_activities")
            .append("div")
            .style("opacity", 0)
            .attr("class", "tooltip")
            .style("background-color", "white")
            .style("border", "solid")
            .style("border-width", "2px")
            .style("border-radius", "5px")
            .style("padding", "5px")
            .style("z-index", "1070")
            .style("position", "absolute")

        var mouseover = function (event, d) {
            tooltip
                .style("opacity", 1)
            d3.select(this)
                .style("stroke", "black")
                .style("opacity", 1)
        }
        var mousemove = function (event, d) {
            if (d !== undefined && d.Distance !== 0) {
                tooltip
                    // round the distance with one decimal
                    .html("Distance: " + Math.round(d.Distance / 1000 * 10) / 10 + " km")
                    .style("left", (event.pageX + 10) + "px")
                    .style("top", (event.pageY) + "px")
            } else {
                tooltip
                    .html("Aucune activité")
                    .style("left", (event.pageX + 10) + "px")
                    .style("top", (event.pageY) + "px")
            }
        }

        var mousemoveweek = function (event, d) {
            if (d !== undefined && getSumDistance(d.week) !== 0) {
                tooltip
                    // round the distance with one decimal
                    .html("Distance: " + Math.round(getSumDistance(d.week) / 1000 * 10) / 10 + " km")
                    .style("left", (event.pageX + 10) + "px")
                    .style("top", (event.pageY) + "px")
            } else {
                tooltip
                    .html("Aucune activité")
                    .style("left", (event.pageX + 10) + "px")
                    .style("top", (event.pageY) + "px")
            }
        }

        var mouseleave = function (d) {
            tooltip
                .style("opacity", 0)
            d3.select(this)
                .style("stroke", "none")
                .style("opacity", 0.8)
        }

        function updateVisualization() {

            const rects = svg.selectAll("rect")
                .data(data);

            // Remplissage de la grille
            rects.enter()
                .append("rect")
                .merge(rects)
                .attr("width", cellSize - 2)
                .attr("height", cellSize - 2)
                .attr("x", d => y_width + 1 + Math.floor(d.day / numRow) * cellSize)
                .attr("y", d => x_height + 1 + (d.day % numRow) * cellSize)
                .attr("fill", d => colorScale(d.Distance))
                .attr("rx", 3)
                .on("click", function (event, d) {
                    if (d['Activity ID'] !== undefined) {
                        window.location.href = "leafletD3.html?activityId=" + encodeURIComponent(d['Activity ID']);
                    }
                })
                .on("mouseover", mouseover)
                .on("mousemove", mousemove)
                .on("mouseleave", mouseleave);

            const weeks = svg.selectAll(".week")
                .data(data);

            // Remplissage de la grille pour les recaps par semaines
            weeks.enter()
                .append("rect")
                .merge(weeks)
                .attr("width", cellSize - 2)
                .attr("height", cellSize - 2)
                .attr("x", d => y_width + 1 + Math.floor(d.week) * cellSize)
                .attr("y", x_height + 8 * cellSize)
                .attr("fill", d => colorScale(getSumDistance(d.week)))
                .attr("rx", 3)
                .on("mouseover", mouseover)
                .on("mousemove", mousemoveweek)
                .on("mouseleave", mouseleave);
        }

        function getDayOfYear(date) {
            let start = new Date(date.getFullYear(), 0, 0);
            let diff = date - start;
            let oneDay = 1000 * 60 * 60 * 24;
            return Math.floor(diff / oneDay) + 5;
        }

        // Commencer la semaine le lundi
        function getWeekOfYear(date) {
            let start = new Date(date.getFullYear(), 0, 2);
            let diff = date - start;
            let oneWeek = 1000 * 60 * 60 * 24 * 7;
            return Math.floor(diff / oneWeek) + 1;
        }

        function getSumDistance(week) {
            return d3.sum(data.filter(d => d.week === week), d => d.Distance);
        }

        updateVisualization();
    })
}

drawCalendar();
