var url = "export_112198995/activities.csv"

//vue par défaut
var currentView = 'year';
changeView(currentView);


nb_2km = 0
nb_10km = 0
nb_20km = 0
nb_30km = 0
nb_40km = 0
nb_marathon = 0

function resetAll() {
    resetView();
    resetData();
}

function resetView() {
    d3.select("#network").selectAll("svg").remove();
    d3.select("#network").selectAll("div").remove();
}

function resetData() {
    nb_2km = 0
    nb_10km = 0
    nb_20km = 0
    nb_30km = 0
    nb_40km = 0
    nb_marathon = 0
}


// Fonction pour changer le mode d'affichage
function changeView(view) {
    resetAll();
    currentView = view;
    if(view == 'year') {
        updateGraphYear();  
    } else if (view == 'month') {
        updateGraphMonth();
    }
}

function updateGraphMonth() {
    resetAll();

    // ouvrir le fichier csv sans d3
    d3.csv(url).then( function(data) {
        const monthsData = [];

        // Pour chaque mois, extrayez les données
        data.forEach(function(d) {
            const date = new Date(d['Activity Date']);
            const month = date.toLocaleString('en-US', { month: 'long' }); // 'long' pour obtenir le nom complet du mois
            const index = monthsData.findIndex(item => item.month === month);

            // Si le mois n'est pas déjà dans le tableau, ajoutez-le
            if (index === -1) {
                monthsData.push({
                    month: month,
                    data: [d]
                });
            } else {
                // Sinon, ajoutez les données à ce mois
                monthsData[index].data.push(d);
            }
        });

        // Pour chaque mois, appelez la fonction drawGraph avec les données du mois
        monthsData.forEach(month => {
            resetData();

            // Créer un conteneur pour chaque mois
            const monthContainer = d3.select("#network").append("div")
                .attr("class", "month-container");

            // Ajouter le nom du mois en tant que titre
            monthContainer.append("h3")
                .text(month.month);

            // Appeler la fonction drawGraph avec les données du mois
            drawGraph(month.data, monthContainer.node());
        });
    });
}


function updateGraphYear() {
    d3.select("#network").selectAll("svg").remove();

    //envoyer les données de l'année à la fonction drawGraph
    d3.csv(url).then( function(data) {
        drawGraph(data);
    });
    
}

//fonction de calcule des différents types de courses
function calculeTypeCourse(data) {
    data.forEach(function(d) {
        d.distance = parseInt(d.Distance);
        //console.log(d.distance)

        // Calcule des différents types de courses
        if (d.distance < 2000) {
            nb_2km += 1
        } else if (d.distance < 10000) {
            nb_10km += 1
        } else if (d.distance < 20000) {
            nb_20km += 1
        } else if (d.distance < 30000) {
            nb_30km += 1
        } else if (d.distance < 40000) {
            nb_40km += 1
        } else if (d.distance > 42000) {
            nb_marathon += 1
        }
           
    });
}

// fonction pour dessiner un graph 
function drawGraph(data, container) {
    
    calculeTypeCourse(data);



    
    const centerNode = { id: "Nombre de courses", size: data.length };

    const nodesData = [
        centerNode,
        { id: "Nombre de 2km", size: nb_2km },
        { id: "Nombre de 10km", size: nb_10km },
        { id: "Nombre de 20km", size: nb_20km },
        { id: "Nombre de 30km", size: nb_30km },
        { id: "Nombre de 40km", size: nb_40km },
        { id: "Nombre de Marathon", size: nb_marathon }
    ];

    // Définir une échelle pour la taille des noeuds
    const scale = d3.scaleLinear()
        .domain([0, d3.max(nodesData.slice(1), d => d.size)])
        .range([10, 20]);

    const linksData = nodesData.slice(1).map(node => ({ source: centerNode.id, target: node.id }));


    // Créer un élément SVG
    const svg = d3.select("#network").append("svg")
        .attr("width", 600)
        .attr("height", 200);
        

    const svgWidth = parseInt(svg.attr("width"));
    const svgHeight = parseInt(svg.attr("height"));
    const minSvgDimension = Math.min(svgWidth, svgHeight);
    const radius = minSvgDimension * 0.4;

    const numNodes = nodesData.length - 1;

    // Condition pour créer les liens uniquement si la valeur est différente de zéro
    const nonZeroLinksData = linksData.filter(link => nodesData.find(node => node.id === link.target).size !== 0);

    const links = svg.selectAll("line")
        .data(nonZeroLinksData)
        .enter().append("line")
        .style("stroke", "gray")
        .style("stroke-width", 2);

    // relier le noeud central aux noeuds périphériques
    links.attr("x1", svgWidth / 2)
    .attr("y1", svgHeight / 2)
    .attr("x2", (d, i) => {
        if (d.target === "Nombre de Marathon") {
            return svgWidth / 2 + radius * Math.cos((2 * Math.PI * (i + 1)) / numNodes);
        } else {
            return svgWidth / 2 + radius * Math.cos((2 * Math.PI * i) / numNodes);
        }
    })
    .attr("y2", (d, i) => {
            return svgHeight / 2 + radius * Math.sin((2 * Math.PI * i) / numNodes);
    });
    

    const nodes = svg.selectAll("g")
        .data(nodesData)
        .enter().append("g")
        .attr("transform", function(d, i) {
            if (d.id === centerNode.id) {
                return `translate(${svgWidth / 2},${svgHeight / 2})`;
            } else {
                const angle = (2 * Math.PI * (i - 1)) / numNodes;
                const x = svgWidth / 2 + radius * Math.cos(angle);
                const y = svgHeight / 2 + radius * Math.sin(angle);
                return `translate(${x},${y})`;
            }
        });

    // Définir une échelle de couleurs
    const colorScale = d3.scaleOrdinal(d3.schemeCategory10);

    // Condition pour créer les cercles uniquement si la valeur est différente de zéro
    nodes.filter(d => d.size !== 0)
        .append("circle")
        .attr("r", d => scale(d.size))
        .style("fill", (d, i) => d.id === centerNode.id ? "grey" : colorScale(d.id));



        // Condition pour créer le texte uniquement si la valeur est différente de zéro
        nodes.filter(d => d.size !== 0)
            .append("text")
            .attr("text-anchor", "middle")
            .attr("dy", function(d) {
                const yOffset = d.id === centerNode.id ? 5 : 0;
                return yOffset + (d.id === centerNode.id ? 0 : + 5);
            })
            .text(function(d) {
                return d.size;
            });

    

    // Ajouter un tooltip pour afficher le nom du noeud
    nodes.append("title")
        .text(d => d.id + " : " + d.size);

    // Ajouter une légende
    const legend = svg.append("g")
        .attr("class", "legend")
        .attr("transform", `translate(${svgWidth - 170}, 50)`);  // Ajuster la position de la légende

    const legendItems = legend.selectAll(".legend-item")
        .data(nodesData.filter(d => d.size !== 0))
        .enter().append("g")
        .attr("class", "legend-item")
        .attr("transform", (d, i) => `translate(0, ${i * 20})`);

    // Utiliser la même échelle de couleurs pour la légende
    legendItems.append("circle")
        .attr("r", d => 5) 
        .style("fill", (d, i) => d.id === centerNode.id ? "grey" : colorScale(d.id));

    legendItems.append("text")
        .attr("x", 15)  // Ajuster la position du texte pour éviter le chevauchement avec le cercle
        .attr("y", 5)
        .text(d => d.id);


}